﻿Imports Mes.WindowsService
Imports System.IO
Imports System.Data.SqlClient

Public Class DataCollectorService
    Inherits Mes.WindowsService.MesWindowsService
    Private Shared ReadOnly _log As New Mes.Diagnostics.Logger.LogCore(
        System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Public MachineList As New List(Of Machine)
    'Public debugString As String

    ' The main entry point for the process
    <MTAThread()>
    Shared Sub Main(ByVal args As String())
        InitializeWindowsService(Of DataCollectorService)(args)
    End Sub

    Public Sub New()
        MyBase.New()
        InitializeComponent()
    End Sub

    Private Sub InitializeComponent()
        '
        'DataCollectorService
        '
        Me.DisplayName = "HSM Data Collector Service"
        Me.ServiceName = "HSM.DataCollector.Service"
    End Sub

    Protected Overrides Sub OnStart(args() As String)
        MyBase.OnStart(args)
        'debugString = "Starting service."
        Dim mType As Integer = 0
        Dim mIP As String = ""
        Dim mName As String = ""
        Dim machineID As Integer = 0
        Dim lineCount As Integer = 1
        Dim path As String = System.AppDomain.CurrentDomain.BaseDirectory + "MachineList.txt"
        Try 'read machines from txt file and begin polling data from them
            _log.Info("Starting the service. Proceeding to log machines and initialize param dictionaries.")
            Using reader As StreamReader = My.Computer.FileSystem.OpenTextFileReader(path)
                Dim a As String
                Do 'read the machineList.txt file line by line, storing each line in "a"
                    a = reader.ReadLine
                    If lineCount = 1 Then
                        mType = CInt(a) 'this value of a will be either "1" (assembler) or "2" (coiler) 
                        lineCount = lineCount + 1
                    ElseIf lineCount = 2 Then
                        mName = a
                        lineCount = lineCount + 1
                    ElseIf lineCount = 3 Then
                        mIP = a
                        machineID = LogMachineToSQL(mType, mIP, mName) 'Insert new machine into the database, get back the inserted ID
                        'Dim mach As Machine = New Machine(machineID, mType, mIP, mName)
                        'MachineList.Add(mach) 'Add the new machine to the MachineManager list
                        MachineList.Add(New Machine(machineID, mType, mIP, mName)) 'Add the new machine to the MachineManager list
                        _log.Info("LOGGED: Machine Name: " & mName & ", Machine ID: " & machineID & ", Machine Type: " & mType & ", Machine IP: " & mIP)
                        'debugString = "New machine saved successfully."
                        'End If
                        lineCount = 1
                    End If
                Loop Until a Is Nothing
            End Using
            For Each mach In MachineList
                mach.Start() 'start this machine's thread which will infinite loop DoIt to poll the data
            Next
        Catch ex As Exception ' failures probably from a sql connection problem
            _log.Error(ex.Message)
        End Try
    End Sub

    '========================================================================================
    ' If the service stops, we need to stop the threads and close out all connections
    '========================================================================================
    Protected Overrides Sub OnStop()
        MyBase.OnStop()
        _log.Info("Stopping the service. Proceeding to stop each machine's thread.")
        For Each mach As Machine In MachineList
            mach.Stop() 'start this machine's thread which will infinite loop DoIt which polls the data
        Next
    End Sub

    Private Sub DataCollectorService_ShowViewer(serviceConnector As MesWindowsServiceConnector) Handles Me.ShowViewer
        Dim w As New ServiceWindow
        w.Show()
    End Sub

    '========================================================================================
    'The LogMachineToSQL Sub routine is within the FrmMachineConfig class and it completes the 
    'action of opening a SQL connection and inserting a new record in the Machine table with
    'the data for this new machine.
    '========================================================================================
    Private Function LogMachineToSQL(mType As Integer, mIP As String, mName As String) As Integer
        Dim machineExists As Integer
        Dim connectionString As String = "Server = localhost\SQLEXPRESS; Database = MachineData; Integrated Security =  SSPI"
        Try
            Using sqlCon As New SqlConnection(connectionString)
                sqlCon.Open() 'open sql connection
                Using Com As New SqlCommand("EXEC LogMachine @machineType = '" & mType & "', 
                @machineName = '" & mName & "', @ipAddress = '" & mIP & "'", sqlCon)
                    Using reader = Com.ExecuteReader()
                        If reader.HasRows Then 'This S.P. may return a result if the machine already exists
                            Do While reader.Read
                                machineExists = CInt(reader.Item(0))    'returns -1 if the machine exists already
                            Loop
                        End If
                    End Using ' dispose of sqlDataReader resource
                End Using ' dispose of sql command resource
            End Using ' dispose of sqlCon
        Catch ex As Exception
            _log.Error("Error while logging the new machine to the DB: " & ex.Message)
        End Try
        Return machineExists
    End Function
End Class
