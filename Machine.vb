﻿Imports System.Threading
Imports System.Data.SqlClient
Imports CMZ.PPCCom

Public Class Machine
    Private Shared ReadOnly _log As New Mes.Diagnostics.Logger.LogCore(
        System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)
    Private mID As Integer
    Private mType As Integer
    Private mIP As String
    Private mName As String
    Private dict As Dictionary(Of String, Integer) = New Dictionary(Of String, Integer)
    Private worker As Thread
    Private _stopping As Boolean
    Dim readTime As DateTime
    Private connectionString As String = "Server = localhost\SQLEXPRESS; Database = MachineData; Integrated Security =  SSPI"

    Public Sub New(machineID As Integer, machineType As Integer, machineIP As String, machineName As String)
        mID = machineID
        mType = machineType
        mIP = machineIP
        mName = machineName
        GetParamList()
        readTime = DateAdd(DateInterval.Hour, -2, Date.Now) 'to allow the first prod count read immediately
    End Sub

    Public Sub Start()
        worker = New Thread(AddressOf DoIt)
        worker.Start()
    End Sub

    Public Sub [Stop]()
        If worker IsNot Nothing Then
            _stopping = True
            worker.Join()
        End If
    End Sub
    ' -------------------------------------------------------------------------------------------------------------
    ' The Machine object runs a thread which points at this DoIt method. It runs an infinite loop, 
    ' polling data from the machine. It will log alarm events and HMI param changes.
    ' -------------------------------------------------------------------------------------------------------------
    Private Sub DoIt()
        Dim readValue As Integer = 0 'all param values are returned as a string. value can be interpreted differently depending if type is boolean or int
        Dim recipeName As String = ""
        Dim wireDiameter As Integer = 0
        Dim springDiameter As Integer = 0
        Dim coilsPerRow As Integer = 0
        Dim numberOfRows As Integer = 0
        Dim paramName As String = ""
        Dim paramValue As String = ""
        'Dim hourDiff As Long
        Dim Ppc As New PPCCom
        Dim comResult As PPCComResult
        Dim type As IECType
        Dim typeClass As IECTypeClass
        Dim elements As UInteger
        Dim pair As KeyValuePair(Of String, Integer)
        Dim ConnectionID As UInteger
        Dim firstRun As Boolean = True
        Dim disconnectedflag As Boolean = False
        Dim alarmMsgArray = New ArrayList()

        ' =============================================================================
        ' Infinite loop for this machine's thread. Detects machine alarm messages and HMI param changes 
        ' =============================================================================
        Do Until _stopping
            Try
                comResult = Ppc.Connect(Me.mIP, ConnectionID) ' if connection fails, this thread stops on this line for ~20s until recognizing disconnected
                If Ppc.Connected And dict IsNot Nothing Then
                    disconnectedflag = True
                    ' =============================================================================
                    ' First read the current alarm message to log an alarm
                    ' =============================================================================
                    comResult = Ppc.Read("MesageList", readValue) 'MesageList is an integer value of which alarm is showing on HMI

                    'Log unique alarm messages by adding to array if it does not exist already and call LogAlarmEvent sp
                    If Not (alarmMsgArray.Contains(readValue)) And firstRun = False Then
                        alarmMsgArray.Add(readValue)

                        ' Get recipe name
                        If machineType = 1 Then
                            comResult = Ppc.Read("NameOfCurrentRecipe", recipeName)
                            'comResult = Ppc.Read("YayCapi", wireDiameter)
                            'comResult = Ppc.Read("YayCapi", springDiameter)
                            'comResult = Ppc.Read("ToplamsiraAdeti", numberOfRows)
                            'comResult = Ppc.Read("SiraiciYayAdeti", coilsPerRow)
                            'recipeName = Format((CDbl(wireDiameter) / 10.0) * 0.03937007874, "n3") + "-" + CStr(springDiameter) + " " + CStr(numberOfRows) + "x" + CStr(coilsPerRow)
                        Else ' If machine is a Coiler, we will form the recipe name using the wire and spring diameter
                            'comResult = Ppc.Read("G_ReceteIndex", recipeName)
                            comResult = Ppc.Read("TelCapiHMI", wireDiameter)
                            comResult = Ppc.Read("YayCapiHMI", springDiameter)
                            If springDiameter = 570 Then
                                springDiameter = 900
                            ElseIf springDiameter = 650 Then
                                springDiameter = 800
                            End If

                            recipeName = CStr(Decimal.Round(CDec(CDbl(wireDiameter) / 10 * 0.03937007874), 2, MidpointRounding.AwayFromZero)).Replace(".", "") + "-" + CStr(springDiameter)
                        End If
                        '=======================================================================
                        Using sqlCon As New SqlConnection(connectionString)
                            sqlCon.Open() 'open sql connection
                            Using Com As New SqlCommand("LogAlarmEvent", sqlCon)
                                Com.CommandType = System.Data.CommandType.StoredProcedure
                                Com.Parameters.AddWithValue("@mID", mID)
                                Com.Parameters.AddWithValue("@alarmID", CStr(readValue)) 'send integer mesageList/alarmID to DB
                                Com.Parameters.AddWithValue("@recipeName", recipeName)
                                Using reader = Com.ExecuteReader()
                                End Using 'disposes of the reader resource
                            End Using 'disposes of the sql command
                            '_log.Info("Alarm Message " & readValue & " added")
                        End Using
                    End If
                    ' =============================================================================
                    ' Loop through param cache 'dict' to compare the existing param to the new one we read
                    ' =============================================================================
                    For Each pair In dict.ToList 'loop through dictionary of HMI params and check if they changed
                        ' Param Name = pair.Key
                        ' Param value = pair.Value
                        comResult = Ppc.GetVarInfo(pair.Key, type, typeClass, elements) 'Get the data type
                        comResult = Ppc.Read(pair.Key, readValue) 'store the param value into readValue
                        ' =============================================================================
                        ' Compare the param value we just read to the value in cache. If they are different, update SQL db & dict
                        ' =============================================================================
                        'hourDiff = Math.Abs(DateDiff(DateInterval.Minute, System.DateTime.Now, readTime))
                        If (readValue <> pair.Value Or firstRun = True) And Ppc.Connected Then

                            ' Clear array if we read either AutoMotionStart or AutomaticStart
                            If (pair.Key = "AutoMotionStart" Or pair.Key = "AutomaticStart") Then
                                alarmMsgArray.Clear()
                                '_log.Debug("Clearing alarm message array")
                            End If
                            '=====================================================================

                            If (pair.Key = "ToplamYatakUretimAdeti" Or pair.Key = "MakinaYayUretimAdeti") And Math.Abs(DateDiff(DateInterval.Second, Date.Now, readTime)) < 300 Then
                                Continue For 'Only read in the production count every X hours to save DB space
                            ElseIf (pair.Key = "ToplamYatakUretimAdeti" Or pair.Key = "MakinaYayUretimAdeti") And Math.Abs(DateDiff(DateInterval.Second, Date.Now, readTime)) >= 300 Then
                                readTime = Date.Now 'update the read time after we log the production count at 5 minutes (300s)
                            End If

                            dict(pair.Key) = readValue 'update the cache of existing param values

                            If type = 1 Then 'type 1 = BOOLEAN... convert to TRUE/FALSE before updating DB
                                If readValue = 1 Then 'value of 1 = TRUE, 0 = FALSE
                                    paramValue = "True"
                                Else
                                    paramValue = "False"
                                End If
                            Else ' else the datatype is an INT or DINT, convert to String before updating DB
                                paramValue = CStr(readValue)
                            End If
                            ' =============================================================================
                            ' Execute the stored procedure LogParamEvent to save new paramValue to the DB
                            ' =============================================================================
                            Using sqlCon As New SqlConnection(connectionString)
                                sqlCon.Open() 'open sql connection
                                Using Com As New SqlCommand("LogParamEvent", sqlCon)
                                    Com.CommandType = System.Data.CommandType.StoredProcedure
                                    Com.Parameters.AddWithValue("@mID", mID)
                                    Com.Parameters.AddWithValue("@paramValue", paramValue)
                                    Com.Parameters.AddWithValue("@paramName", pair.Key)
                                    Using reader = Com.ExecuteReader()
                                    End Using 'disposes of the reader resource
                                    '_log.Info("Logging ParamEvent " & paramName & " with value " & paramValue)
                                End Using 'disposes of the sql command

                            End Using 'disposes of sqlCon
                            ' =============================================================================
                        End If ' readValue <> pair.Value
                    Next 'loop through machine's dictionary of param values, For Each pair In dict
                    firstRun = False
                ElseIf dict Is Nothing Then
                    GetParamList() 'If we don't already have a cache of param values, initialize it now
                ElseIf Not Ppc.Connected And disconnectedflag = False Then
                    disconnectedflag = True
                    _log.Error(machineName & " was disconnected at " & CStr(Date.Now))
                End If ' If connection with machine was successful, Ppc.Connected
                Thread.Sleep(TimeSpan.FromMilliseconds(250))

            Catch ex As Exception
                _log.Error(ex.Message)
            End Try
        Loop
        ' clean up / close things
    End Sub

    Property machineID As Integer
        Get
            Return mID
        End Get
        Set(value As Integer)
            mID = value
        End Set
    End Property

    Property machineType As Integer
        Get
            Return mType
        End Get
        Set(value As Integer)
            mType = value
        End Set
    End Property

    Public Property machineIP As String
        Get
            Return mIP
        End Get
        Set(value As String)
            mIP = value
        End Set
    End Property

    Public Property machineName As String
        Get
            Return mName
        End Get
        Set(value As String)
            mName = value
        End Set
    End Property
    ' -------------------------------------------------------------------------------------------------------------
    ' The GetParamList method will populate the class variable Dict with initial values for the param cache.
    ' This method keep trying to populate Dict until we successfully connect to the machine, then we won't run this.
    ' -------------------------------------------------------------------------------------------------------------
    Private Sub GetParamList()
        Dim paramValue As Integer = 0
        Dim paramName As String = ""
        Dim Ppc As New PPCCom
        Dim comResult As PPCComResult
        Dim ConnectionID As UInteger
        Try
            comResult = Ppc.Connect(mIP, ConnectionID) 'connect to the machine
            If Ppc.Connected Then
                Using sqlCon As New SqlConnection(connectionString)
                    sqlCon.Open() 'open sql connection
                    Using Com As New SqlCommand("GetParamList", sqlCon)
                        Com.CommandType = System.Data.CommandType.StoredProcedure
                        Com.Parameters.AddWithValue("@mType", mType)
                        Using reader = Com.ExecuteReader()
                            If reader.HasRows Then
                                Do While reader.Read 'this will read each param name for the given machine
                                    paramName = reader.Item(0).ToString()
                                    comResult = Ppc.Read(paramName, paramValue)
                                    dict.Add(paramName, CInt(paramValue)) ' store the param that we read from SQL into this dictionary
                                    'reader.Read()
                                Loop
                            End If 'We finished reading all rows from the recordset
                        End Using 'disposes of the reader resource
                        '_log.Info("Logging ParamEvent " & paramName & " with value " & paramValue)
                    End Using 'disposes of the sql command
                End Using ' disposes of sqlCon
                _log.Info("Created param dictionary for " & machineName)
            End If
        Catch ex As Exception
            _log.Error(ex.Message)
        End Try
    End Sub

End Class
